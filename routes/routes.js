const express = require('express');
const router = express.Router();
const stack = require('../modules/stack');

router.get('/list',stack.listStack);  // Use query as option to get data from different functionalities i,e list?option=stack or list without option
router.post('/push',stack.pushStack); // Can also send element to push as params here i,e /push/:element
router.post('/pop',stack.popStack);

module.exports = router;