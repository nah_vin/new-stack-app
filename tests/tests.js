const expect = require("chai").expect;
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
// const stackModule = require('../modules/stack');

chai.use(chaiHttp);

before(async () => {
})

const listElements = async (option)=>{
    return await chai.request(server)
    .get(`/api/list?option=${option}`)
    .send({})
}

const popElement = async (option)=>{
   return await chai.request(server)
    .post(`/api/pop?option=${option}`)
    .send({})
}

const pushElement = async (option)=>{
    return await chai.request(server)
    .post(`/api/push?option=${option}`)
    .send({ element: 1 })
}

describe('Happy path', () => {
    it('Push element to class: ', async () => {
        let res = await pushElement("stack")
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('List stack from class: ', async () => {
        let res = await listElements("stack")
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object').to.have.property('data');
    })
    it('Push element to array: ', async () => {
        let res = await pushElement()
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('List stack from array: ', async () => {
        let res = await listElements()
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object').to.have.property('data');
    })
    it('Pop element from class: ', async () => {
        let res = await popElement("stack")
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('Pop element from array: ', async () => {
        let res = await popElement()
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
})
describe('Not so happy, but it\'s okay', () => {
    it('List empty class stack: ', async () => {
        let res = await listElements("stack")
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('List empty array stack: ', async () => {
        let res = await listElements()
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('Pop element from empty array: ', async () => {
        let res = await popElement()
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('Pop element from empty array: ', async () => {
        let res = await popElement("stack")
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('Push element to stack with invalid request: ', async () => {
        let res =  await chai.request(server)
        .post(`/api/push`)
        .send({})
        expect(res.status).equal(400);
        expect(res.body).to.be.an('object').to.have.property('message').to.equal('Bad request');
    })
})