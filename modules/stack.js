class Stack{
    #stack
    #count
    constructor(){
        this.#stack = []
        this.#count = 0
    }
    push(element){
        this.#stack[this.#count] = element;
        this.#count +=1
        return this.#stack;
    }
    pop(){
        if(this.#count == 0){
            return {message:`Stack is empty : ${this.#stack}`};
        }else{
            this.#stack[this.#count - 1]
            this.#count -=1
            return this.#stack[this.#count - 1];
        }  
    }
    list(){
        return this.#stack;
    }
}

let stack = new Stack();
// console.log(stack.list());
// console.log(stack.push(1));
// console.log(stack.pop());
// console.log(stack.push(1));
// console.log(stack.push(7));
// console.log(stack.push(31));
// console.log(stack.push(5));
// console.log(stack.push(9));
// console.log(stack.push(3));
// console.log(stack.pop());
// console.log(stack.list());

let stackArray = [];

async function listStack(req,res){
    if(req.query.option === "stack"){
        let data = stack.list()
        return res.status(200).json({message:`Stack from class`, data:data});
    }else{
        return res.status(200).json({message:`Stack from array`,data:stackArray});
    }
}
async function popStack(req,res){
    function removeElement(stack){
        let element
        if (stack.length == 0) {
            return res.status(200).json({message:`Stack is empty : ${stack}`});
        }else{
            element = stack.pop();
            return res.status(200).json({message:element});
        }
    }
    (req.query.option === "stack") ? removeElement(stack) :  removeElement(stackArray)   
}
async function pushStack(req,res){
    function addElement(stack){
        if(!req.body.element || Object.keys(req.body).length == 0) return res.status(400).json({message:`Bad request`});
        stack.push(req.body.element);
        return res.status(200).json({message:`Pushed element : ${req.body.element}`});
    }
    (req.query.option === "stack") ? addElement(stack) :  addElement(stackArray)   
}


module.exports={
    listStack,
    popStack,
    pushStack
}



