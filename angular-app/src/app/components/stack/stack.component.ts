import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-stack',
  templateUrl: './stack.component.html',
  styleUrls: ['./stack.component.css']
})
export class StackComponent implements OnInit {
 
  element : any
  constructor(private stack:ServicesService) {
   }
   stackData = {};

  ngOnInit(): void {
    this.stack.getLists().subscribe(res =>{  
      this.stackData = res
    });
  }
 
  getLists(){
    this.stack.getLists().subscribe(res =>{  
      this.stackData = res
    });
  }
  pushElement(){
    const ele = {
      element : this.element
    }
    this.stack.pushElements(ele).subscribe(res =>{  
      if(res) this.getLists()
    },
    err=>{
      if(err.status == 400)
      alert(JSON.stringify(err.error));
    });
  }
  popElement(){
    this.stack.popElements().subscribe(res =>{ 
      alert(JSON.stringify(res));
      if(res) this.getLists()   
    },
    err=>{
      alert(JSON.stringify(err.error));
    });
  }
}
