import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ServicesService {
  constructor(private http: HttpClient) { }

  getLists() {
    return this.http.get('http://localhost:8000/api/list');
  }
  pushElements(ele:any) {
    return this.http.post('http://localhost:8000/api/push',ele,httpOptions);
  }
  popElements() {
    return this.http.post('http://localhost:8000/api/pop',httpOptions);
  }
}
